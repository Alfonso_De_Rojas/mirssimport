
CREATE TABLE Usuarios (
Id int  PRIMARY KEY,
Nombre Varchar(100) NOT NULL,
Email varchar(200) NOT NULL,
Telefono varchar(200) NOT NULL,
Direccion varchar(100) NOT NULL,
Cod_Postal varchar(100) NOT NULL,
Ciudad varchar(100) NOT NULL
)

CREATE TABLE Categoria (
Id int PRIMARY KEY,
Nombre Varchar(100) NOT NULL
)

CREATE TABLE Post (
Id int PRIMARY KEY,
Titulo Varchar (100) NOT NULL,
Fecha datetime NOT NULL,
Cuerpo varchar (8000) NOT NULL,
Autor varchar (100) NOT NULL,
Idioma varchar (5) NOT NULL,
link varchar (250) NOT NULL 
)


