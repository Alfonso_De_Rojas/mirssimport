package com.cesur;
import java.util.List;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
/**
 * Hello world!
 *
 */
public class App 
{
    private static String serverDB="localhost";
    private static String portDB="1533";
    private static String DBname="RSSimport";
    private static String userDB="sa";
    private static String passwordDB="12345Ab##";
    public static String url ="https://lenguajedemarcasybbdd.wordpress.com/feed/";
    /** 
     * @param args
     */
    public static void main( String[] args )
    {
        
        
        Scanner sc = new Scanner (System.in);
        boolean salir=false;
        while (salir !=true){
        System.out.println("--------------Menu-------------");
        System.out.println("-------------------------------");
        System.out.println("|0. Importar rss y crear BBDD  |");
        System.out.println("|1. Ultimos 10 posts           |");
        System.out.println("|2. ver todos los posts        |");
        System.out.println("|3. Buscar por titulo o palabra|");
        System.out.println("|4. Filtrar por categoria      |");
        System.out.println("|5. Salir                      |");
        System.out.println("-------------------------------");

        XmlParse x =new XmlParse();
        int menu=sc.nextInt();
        switch (menu){
            //importar rss
            case 0:
                System.out.println("Por favor, introduzca la url del rss que desea usar");
                url=sc.nextLine();
                
                bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
                b.modificarBBDD("insert into Usuarios values ('1','bernat','bernat@cesur.com','666666666','calle falsa 13','41008','Sevilla')");
                b.modificarBBDD("insert into Usuarios values ('2','belen','belen@cesur.com','666666666','calle falsa 13','41008','Sevilla')");
                b.modificarBBDD("insert into Usuarios values ('3','Manuel','manuermejo@cesur.com','666666666','calle falsa 13','41008','Sevilla')");
                b.modificarBBDD("insert into Usuarios values ('4','paco','paco@cesur.com','666666666','calle falsa 13','41008','Sevilla')");
                b.modificarBBDD("insert into Usuarios values ('5','pepe','pepe@cesur.com','666666666','calle falsa 13','41008','Sevilla')");
                b.modificarBBDD("insert into Usuarios values ('6','Francisco','francisco@cesur.com','666666666','calle falsa 13','41008','Sevilla')");
                b.modificarBBDD("insert into Usuarios values ('7','Almudena','almudena@cesur.com','666666666','calle falsa 13','41008','Sevilla')");
                b.modificarBBDD("insert into Usuarios values ('8','Carmen','carmen@cesur.com','666666666','calle falsa 13','41008','Sevilla')");
                b.modificarBBDD("insert into Usuarios values ('9','Antonia','antonia@cesur.com','666666666','calle falsa 13','41008','Sevilla')");
                b.modificarBBDD("insert into Usuarios values ('10','Alfonso','Arfonsitoermejo@cesur.com','666666666','calle falsa 13','41008','Sevilla')");
                b.modificarBBDD("insert into Categoria values('1','Miedo')");
                b.modificarBBDD("insert into Categoria values('2','Humor')");
                b.modificarBBDD("insert into Categoria values('3','Politica')");
                b.modificarBBDD("insert into Categoria values('4','guerras')");
                b.modificarBBDD("insert into Categoria values('5','paises')");
                b.modificarBBDD("insert into Categoria values('6','Electronica')");
                b.modificarBBDD("insert into Categoria values('7','Comercios')");
                b.modificarBBDD("insert into Categoria values('8','Local')");
                b.modificarBBDD("insert into Categoria values('9','Mundial')");
                b.modificarBBDD("insert into Categoria values('10','Estudio ')");
                b.modificarBBDD("insert into Post values ('1','Tiroteo en EEEUU','2014-12-04','Este medio día se produjo un gran tiroteo en la ciudad de Idaho, en la que salieron heridas 6 personas y fallecieron 3 niños, aun se desconoce el motivo del tiroteo, pero se está investigando el caso.','Alan Brito','ES','https://www.noticasfalsas.com/tiroteo')");
                b.modificarBBDD("insert into Post values ('2','Robo','09-04-2017','Hoy hemos sabido de un nuevo intento de robo en la tienda de Pepito Grillo, por lo visto los ladrones simplemente se llevaron unos tapones para los oidos','Al K. Huete','ES','https://www.noticasfalsas.com/Robo')");
                b.modificarBBDD("insert into Post values ('3','Futbol','2016-10-04','Hoy en los deportes hemos visto como ha sido la presentación de el gran fichaje de Joaquin por el ASIR.FC, después de hacer una gran temporada con Real DAW, ha decidido marcharse por 2 palmeras caducadas.','Alex Plosivo','EN','https://www.noticasfalsas.com/Futbol')");
                b.modificarBBDD("insert into Post values ('4','Guerra','2018-09-04','Desgraciadamente hoy nos despertamos con una terrible noticia, el terrorista mas buscado por la CIA,Osama Fonsiladen, ha hecho explotar uno de los cementerios nucleares mas grandes del mundo en Rusia','Andrés Tresado','PT','https://www.noticasfalsas.com/Guerrea')");
                b.modificarBBDD("insert into Post values ('5','Pelicula','2021-09-04','Ya conocemos el estreno de la nueva pelicula de la gran saga de coches Fast and Furious 9, el estreno será el proximo 9 de Julio','Manuel Jurado','ES','https://www.noticasfalsas.com/Pelicula')");
                b.modificarBBDD("insert into Post values ('6','Estudios','10-10-2019','Ya nos preparamos para una de las epocas del año mas importantes para los niños, la vuelta al cole, muchos son los padres que compran las ultimas cosas para sus niños y las tiendas están desbordadas','Ezequiel','ES','https://www.noticasfalsas.com/Estudios')");
                b.modificarBBDD("insert into Post values ('7','Covid','2019-09-04','COMUNICADO OFICAL! SE DECLARA EL ESTADO DE ALARMA EN ESPAÑA, QUEDA PROHIBIDO SALIR SUS CASAS A NO SER QUE SEA CON MOTIVO JUSTIFICADO.','Pedro Sanchez','ES','https://www.noticasfalsas.com/Covid')");
                b.modificarBBDD("insert into Post values ('8','Cesur y sus escasos materiales','2020-02-16','Los alumnos del centro de enseñanza profesional Cesur, se quejan porque las clases no tienen los materiales necesitarios o indispensables para llevar a cabo una buena clase','Fran Campos','ES','https://www.noticasfalsas.com/Cesurysusescasosmateriales')");
                b.modificarBBDD("insert into Post values ('9','El alcalde del mundo','2021-09-04','El alcalde del mundo ha declarado esta mañana que todo esto del covid era una tontería, y lo unico que quería es tener un poco más de tiempo para el y salir a la calle sin mucha gente','Bernat','ES','https://www.noticasfalsas.com/Elalcaldelmundo')");
                b.modificarBBDD("insert into Post values ('10','Trabajo','2017-09-04','Debido a esta gran epidemia, hay muchas personas que se han encontrado en situación de paro, o de ERTE y necesitan alguna forma de obtener dinero para mantener a sus familias.','Gonzalo','ES','https://www.noticasfalsas.com/tiroteo')");

                
                
                
                //selecciona del arraylist el que tiene la id 1
               
                break;
              
            //ultimos 10 posts
            case 1:
            System.out.println("Los ultimos 10 post son:");
            List <String> ultimospost =x.leer("rss/channel/item/description/text()",url);
            for(int i = 0; i < 10; i++){
                System.out.println(ultimospost.get(i));
            }
            break;
            //ver todos los post
            case 2:
            List <String>  listapoststodos= x.leer("rss/channel/item/description/text()",url);
            for (int i=0;i<listapoststodos.size();i++){
                System.out.println(listapoststodos.get(i));
            }
            break;
            //buscar por titulo
            case 3:
            String titulo;
            System.out.println("¿Que titulo desea buscar?");
            titulo = sc.nextLine();
            List <String>  listaportitulo= x.leer("rss/channel/item/title='"+titulo+"'|/description/text()",url);
            for (int i=0;i<listaportitulo.size();i++){
                System.out.println(listaportitulo.get(i));
            }
            break;
            //filtrar por categoria
            case 4:
            System.out.println("¿Que categoria desea buscar?");
            String categoria = sc.nextLine();
            categoria.toLowerCase();
            List <String>  listapostcategoria= x.leer("rss/channel/item/title='"+categoria+"'|/description/text()",url);
            for (int i=0;i<listapostcategoria.size();i++){
                System.out.println(listapostcategoria.get(i));
            }
            break;
            //salir del menu
            case 5:
                salir=true;
            break;
        }
    }
       
    }


    private static void CrearBBDD()
    {
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
        
        String i= b.leerBBDDUnDato("SELECT count(1) FROM sys.databases  where name='"+DBname+"' ");
       // String cero = "0";
        if (i.equals("0")){
            b.modificarBBDD("CREATE DATABASE " + DBname);
            b=new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
            String query = LeerScriptBBDD("scripts/CrearBBDD.sql");
          
            b.modificarBBDD(query);
            b.modificarBBDD("insert into clientes values ('Angeles','angeles@cesur.com','666666666','calle falsa 13','41008','Sevilla')");
      
        }else{
            String query = LeerScriptBBDD("scripts/CrearBBDD.sql");
            b.modificarBBDD(query);
        }
    }

    private static String LeerScriptBBDD(String archivo){
       String ret = "";
        try {
            String cadena;
            FileReader f = new FileReader(archivo);
            BufferedReader b = new BufferedReader(f);
            while((cadena = b.readLine())!=null) {
                ret =ret +cadena;
            }
            b.close();
        } catch (Exception e) {
        }
       return ret;

    }
}
