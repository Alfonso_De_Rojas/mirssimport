import java.sql.Date;

public class post {
    int id;
    String nombre;
    Date fecha;
    String cuerpo;
    String autor;
    String idioma;
    String link;

    public post(){

    }

    public void setid(int id){
        this.id=id;
    }
    public int getid(){
        return this.id;
    }
    public void setnombre(String nombre){
        this.nombre=nombre;
    }
    public String getnombre(){
        return this.nombre;
    }
    public void setfecha(Date fecha){
        this.fecha=fecha;
    }
    public Date getfecha(){
        return this.fecha;
    }
    public void setcuerpo(String cuerpo){
        this.cuerpo=cuerpo;
    }
    public String getcuerpo(){
        return this.cuerpo;
    }
    public void setautor(String autor){
        this.autor=autor;
    }
    public String getautor(){
        return this.autor;
    }
    public void setidioma(String idioma){
        this.idioma=idioma;
    }
    public String getidioma(){
        return this.idioma;
    }
    public void setlink(String link){
        this.link=link;
    }
    public String getlink(){
        return this.link;
    }
}
